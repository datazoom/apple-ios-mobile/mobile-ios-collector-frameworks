Pod::Spec.new do |spec|

  spec.name         = "BaseCollector"
  spec.version      = "0.1.0"
  spec.platform = :'ios'
  spec.ios.deployment_target = '10.0'
  spec.summary      = "DataZoom framework"
  spec.homepage     = "http://www.datazoom.io"

  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author = { 'Ashok' => 'ashok@datazoom.io' }

  

  spec.ios.vendored_frameworks = "DataZoom.framework"
  
  spec.source       = { :http => "https://gitlab.com/datazoom/apple-ios-mobile/mobile-ios-collector-frameworks/-/tree/master/BaseCollector/0.1.0/DataZoom.zip", :tag => "#{spec.version}" }

end
