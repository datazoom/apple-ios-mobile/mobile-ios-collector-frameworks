## Introduction

Datazoom is a high availability real-time data collection solution. This iOS project builds the framework file that will be distributed. This document summarizes how to integrate the DataZoom framework with applications.
    

Version 1.0  : Static Framework [Tracked with the help of Player's instance, in all required Controllers/Views].


## iOS - Brightcove : Framework Integration.


### Initial setup:

* The DZBrightcoveCollector.framework file should be downloaded from **[Here](https://gitlab.com/datazoom/mobile-ios-demos/mobile-ios-collector-libraries-internal/blob/develop/brightcove-collector/2.4/DZBrightcoveCollector.framework.zip)** .

* Drag and drop the downloaded framework file into the customer's application or manually, by adding it into 'Embedded Binaries/ Linked Frameworks & Libraries' section of the customer application's target.


### Steps for Swift Language based Applications:

* After including the framework file, open the ViewController/View file, where the AVPlayer(native player) is included.

* Import the framework using the following command,

               import DZBrightcoveCollector


* Initialise the framework by passing along the 'Configuration ID',

               DZBrightcovePlayer.dzSharedManager.initNativePlayerWith(configID: <provide config is string>, url: <provide connection url dev/staging>, playerInstance: videoPlayer)
  

* Run the app and observe the events in Amplitude, data corresponding to MOBILE/iPhone in Platform, refers to the events tracked from iPhone.


### Steps for ObjectiveC Language based Applications:

* After including the framework file, Create a bridging header file, to allow interoperability of languages.
    
* open the ViewController/View file, where the AVPlayer(native player) is included.
    
* Import the following,

           #import <AVKit/AVKit.h>

           #import <AVFoundation/AVFoundation.h>

           #import <DZBrightcoveCollector/DZBrightcoveCollector.h>

* Initialise the swift class in the .h file.

                 DZBrightcoveCollector *dzObject;

* In the .m file, allocate using,

                 dzObject = [[DZBrightcoveCollector alloc]init];

                [dzObject initialiseWithConfigId: <provide config is string>, url: <provide connection url dev/staging>, playerInstance: videoPlayer];


* Run the app and observe the events in Amplitude, data corresponding to MOBILE/iPhone in Platform, refers to the events tracked from iPhone.

## Demo Application

* A demo application that shows the usage of this framework is available **[Here](https://gitlab.com/datazoom/mobile-ios-demos/mobile-ios-brightcove-demo.git)** .

## Credit

- Vishnu M P

## Link to License/Confidentiality Agreement
Datazoom, Inc ("COMPANY") CONFIDENTIAL
Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.
NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
Confidentiality and Non-disclosure agreements explicitly covering such access.
The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
